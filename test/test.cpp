#define CATCH_CONFIG_MAIN

#include "catch.hpp"

// game includes
#include "game.hpp"
#include "utils/helper.hpp"

// maze includes
#include "factory/mazeFactory.hpp"
#include "maze/custom/basic.hpp"
#include "maze/custom/random_v1.hpp"
#include "maze/custom/random_v2.hpp"
#include "maze/custom/random_v3.hpp"

// player includes
#include "factory/playerFactory.hpp"
#include "player/seeker/random.hpp"
#include "player/seeker/IAseekerV1.hpp"
#include "player/seeker/IAseekerV2.hpp"
#include "player/hider/random.hpp"
#include "player/hider/ia_hider_v1.hpp"
#include "player/hider/ia_hider_v2.hpp"

#include "maze/block/material/custom.hpp"


namespace Params {

    std::string hiderClassName = "random";
    std::string seekerClassName = "random";
    std::string mazeClassName = "random"; // by default

    unsigned int nGames = 100;
};

// do not change this test!!!
TEST_CASE("check static params", "Game Params")
{
    REQUIRE(GameParams::height == 20);
    REQUIRE(GameParams::width == 20);
    REQUIRE(GameParams::nRounds == 200);
    REQUIRE(GameParams::nSeekers == 2);
    REQUIRE(GameParams::nHiders == 2);
    REQUIRE(GameParams::pctBlock == 0.40);
    REQUIRE(GameParams::pctWood == 0.08);
    REQUIRE(GameParams::pctStone == 0.04);
    REQUIRE(GameParams::pctMetal == 0.02);
}

TEST_CASE("check maze v1", "random_v1"){

    HiderFactory::registerClass<RandomHider>("random");
    SeekerFactory::registerClass<RandomSeeker>("random");
    MazeFactory::registerClass<RandomMaze_v1>("random");

    auto maze = MazeFactory::create(Params::mazeClassName, GameParams::width, GameParams::height);
    maze->build();

    auto cells = maze->getCells();
    for(unsigned int i = 0; i < GameParams::height; i++){
        for(unsigned int j = 0; j < GameParams::width; j++) {
            auto kind = cells[i * GameParams::height + j]->getBlock()->getKind();
            if (kind == 0) std::cout << "_ ";
            else if (kind == 1) std::cout << "X ";
            else{
                std::cout << "M ";
            }
        }
        std::cout << "\n";
    }
    Game game(maze, GameParams::nRounds);

    // then add Players (hiders and seekers)
    for (unsigned int j = 0; j < GameParams::nHiders; j++) {

        std::string hider_name = "H" + std::to_string(j + 1);
        // only need the player representation server side
        std::shared_ptr<Player> hider = HiderFactory::create(Params::hiderClassName, hider_name);
        game.addPlayer(hider);
    }

    for (unsigned int j = 0; j < GameParams::nSeekers; j++) {

        std::string seeker_name = "S" + std::to_string(j + 1);
        // only need the player representation server side
        std::shared_ptr<Player> seeker = SeekerFactory::create(Params::seekerClassName, seeker_name);
        game.addPlayer(seeker);
    }

    // check maze of current created game
    REQUIRE(Helper::isValid(game));
}


TEST_CASE("check maze v2", "random_v2"){

    HiderFactory::registerClass<RandomHider>("random");
    SeekerFactory::registerClass<RandomSeeker>("random");
    MazeFactory::registerClass<RandomMaze_v2>("random");

    auto maze = MazeFactory::create(Params::mazeClassName, GameParams::width, GameParams::height);
    maze->build();

    auto cells = maze->getCells();
    for(unsigned int i = 0; i < GameParams::height; i++){
        for(unsigned int j = 0; j < GameParams::width; j++) {
            auto block = cells[i * GameParams::height + j]->getBlock();
            if (block->getKind() == 0) std::cout << "_  ";
            else if (block->getKind() == 1) std::cout << "X  ";
            else{
                auto material = std::dynamic_pointer_cast<Material>(block);
                if (material->getMaterialKind() == MaterialKind::WOOD)
                    std::cout << "W  ";
                if (material->getMaterialKind() == MaterialKind::STONE)
                    std::cout << "S  ";
                if (material->getMaterialKind() == MaterialKind::METAL)
                    std::cout << "M  ";
            }
        }
        std::cout << "\n";
    }

    Game game(maze, GameParams::nRounds);

    // then add Players (hiders and seekers)
    for (unsigned int j = 0; j < GameParams::nHiders; j++) {
        std::string hider_name = "H" + std::to_string(j + 1);
        // only need the player representation server side
        std::shared_ptr<Player> hider = HiderFactory::create(Params::hiderClassName, hider_name);
        game.addPlayer(hider);
    }

    for (unsigned int j = 0; j < GameParams::nSeekers; j++) {

        std::string seeker_name = "S" + std::to_string(j + 1);
        // only need the player representation server side
        std::shared_ptr<Player> seeker = SeekerFactory::create(Params::seekerClassName, seeker_name);
        game.addPlayer(seeker);
    }

    // check maze of current created game
    REQUIRE(Helper::isValid(game));
}

TEST_CASE("check maze v3", "random_v3"){

    HiderFactory::registerClass<RandomHider>("random");
    SeekerFactory::registerClass<RandomSeeker>("random");
    MazeFactory::registerClass<RandomMaze_v3>("random");

    auto maze = MazeFactory::create(Params::mazeClassName, GameParams::width, GameParams::height);
    maze->build();

    auto cells = maze->getCells();
    for(unsigned int i = 0; i < GameParams::height; i++){
        for(unsigned int j = 0; j < GameParams::width; j++) {
            auto block = cells[i * GameParams::height + j]->getBlock();
            if (block->getKind() == 0) std::cout << "_  ";
            else if (block->getKind() == 1) std::cout << "X  ";
            else{
                auto material = std::dynamic_pointer_cast<Material>(block);
                if (material->getMaterialKind() == MaterialKind::WOOD)
                    std::cout << "W  ";
                if (material->getMaterialKind() == MaterialKind::STONE)
                    std::cout << "S  ";
                if (material->getMaterialKind() == MaterialKind::METAL)
                    std::cout << "M  ";
            }
        }
        std::cout << "\n";
    }
    Game game(maze, GameParams::nRounds);

    // then add Players (hiders and seekers)
    for (unsigned int j = 0; j < GameParams::nHiders; j++) {

        std::string hider_name = "H" + std::to_string(j + 1);
        // only need the player representation server side
        std::shared_ptr<Player> hider = HiderFactory::create(Params::hiderClassName, hider_name);
        game.addPlayer(hider);
    }

    for (unsigned int j = 0; j < GameParams::nSeekers; j++) {

        std::string seeker_name = "S" + std::to_string(j + 1);
        // only need the player representation server side
        std::shared_ptr<Player> seeker = SeekerFactory::create(Params::seekerClassName, seeker_name);
        game.addPlayer(seeker);
    }

    // check maze of current created game
    REQUIRE(Helper::isValid(game));
}

TEST_CASE("check hider victory", "random vs random")
{
    // Need to register expected class
    HiderFactory::registerClass<RandomHider>("random");
    SeekerFactory::registerClass<RandomSeeker>("random");
    MazeFactory::registerClass<BasicMaze>("random");

    std::map<Role, unsigned int> results;
    results[Role::HIDER] = 0;
    results[Role::SEEKER] = 0;

    for (int i = 0; i < Params::nGames; i++) {

        // create Game and init maze (required call build)
        auto maze = MazeFactory::create(Params::mazeClassName, GameParams::width, GameParams::height);
        maze->build();

        Game game(maze, GameParams::nRounds);

        // then add Players (hiders and seekers)
        for (unsigned int j = 0; j < GameParams::nHiders; j++) {

            std::string hider_name = "H" + std::to_string(j + 1);
            // only need the player representation server side
            std::shared_ptr<Player> hider = HiderFactory::create(Params::hiderClassName, hider_name);
            game.addPlayer(hider);
        }

        for (unsigned int j = 0; j < GameParams::nSeekers; j++) {

            std::string seeker_name = "S" + std::to_string(j + 1);
            // only need the player representation server side
            std::shared_ptr<Player> seeker = SeekerFactory::create(Params::seekerClassName, seeker_name);
            game.addPlayer(seeker);
        }

        std::cout << "Game n°" << (i + 1) << " with: "
                  << "{hider: " << Params::hiderClassName
                  << ", seeker: " << Params::seekerClassName
                  << ", maze: " << Params::mazeClassName
                  << "}" << std::endl;

        // before running
        REQUIRE(Helper::isValid(game));
        //if (!Helper::isValid(game)) {
        //    std::cout << "Game is not valid: unexpected maze built..." << std::endl;
        //}

        // run the game
        while (!game.end()) {

            auto player = game.getCurrentPlayer();

            // state is composed of <Observation, vector<PlayerAction>>
            auto state = game.getState(player);
            // get the chosen action by IA
            PlayerAction chosenAction = player->play(state.first, state.second);
            // Play the action in the game and retrieve the new game state
            auto observation = game.step(player, chosenAction);
            // updtae the IA
            player->update(state.first, observation, chosenAction);
        }

        Role winner = game.getCurrentWinner();
        std::cout << "-- Winner is: " << roleToString(winner) << " team in "
                  << game.getNumberOfRounds() << " rounds" << std::endl;

        // update stat
        results[winner] += 1;
    }

    std::cout << "--------------------------------------------" << std::endl;
    std::cout << "End of simulation... Results over " << Params::nGames << " games are: " << std::endl;
    for (auto &k : results) {
        std::cout << " -- " << roleToString(k.first) << ": " << k.second << " wins ("
                  << (k.second / (double)Params::nGames) * 100. << "%)" <<  std::endl;
    }

    // at least 50 percent of victory for hiders
    REQUIRE(results[Role::HIDER] / (double)Params::nGames > 0.5);
}

// ToDo write your own tests:

// - 50 percent of victory vs random with your seeker and/or hider

TEST_CASE("check IAHiderV1 victory", "IAHiderV1 vs random")
{
    // Need to register expected class
    HiderFactory::registerClass<IAHiderV1>("random");
    SeekerFactory::registerClass<RandomSeeker>("random");
    MazeFactory::registerClass<RandomMaze_v1>("random");

    std::map<Role, unsigned int> results;
    results[Role::HIDER] = 0;
    results[Role::SEEKER] = 0;

    for (int i = 0; i < Params::nGames; i++) {

        // create Game and init maze (required call build)
        auto maze = MazeFactory::create(Params::mazeClassName, GameParams::width, GameParams::height);
        maze->build();

        Game game(maze, GameParams::nRounds);

        // then add Players (hiders and seekers)
        for (unsigned int j = 0; j < GameParams::nHiders; j++) {

            std::string hider_name = "H" + std::to_string(j + 1);
            // only need the player representation server side
            std::shared_ptr<Player> hider = HiderFactory::create(Params::hiderClassName, hider_name);
            game.addPlayer(hider);
        }

        for (unsigned int j = 0; j < GameParams::nSeekers; j++) {

            std::string seeker_name = "S" + std::to_string(j + 1);
            // only need the player representation server side
            std::shared_ptr<Player> seeker = SeekerFactory::create(Params::seekerClassName, seeker_name);
            game.addPlayer(seeker);
        }

        std::cout << "Game n°" << (i + 1) << " with: "
                  << "{hider: " << Params::hiderClassName
                  << ", seeker: " << Params::seekerClassName
                  << ", maze: " << Params::mazeClassName
                  << "}" << std::endl;

        // before running
        REQUIRE(Helper::isValid(game));
        //if (!Helper::isValid(game)) {
        //    std::cout << "Game is not valid: unexpected maze built..." << std::endl;
        //}

        // run the game
        while (!game.end()) {

            auto player = game.getCurrentPlayer();

            // state is composed of <Observation, vector<PlayerAction>>
            auto state = game.getState(player);
            // get the chosen action by IA
            PlayerAction chosenAction = player->play(state.first, state.second);
            // Play the action in the game and retrieve the new game state
            auto observation = game.step(player, chosenAction);
            // updtae the IA
            player->update(state.first, observation, chosenAction);
        }

        Role winner = game.getCurrentWinner();
        std::cout << "-- Winner is: " << roleToString(winner) << " team in "
                  << game.getNumberOfRounds() << " rounds" << std::endl;

        // update stat
        results[winner] += 1;
    }

    std::cout << "--------------------------------------------" << std::endl;
    std::cout << "End of simulation... Results over " << Params::nGames << " games are: " << std::endl;
    for (auto &k : results) {
        std::cout << " -- " << roleToString(k.first) << ": " << k.second << " wins ("
                  << (k.second / (double)Params::nGames) * 100. << "%)" <<  std::endl;
    }

    // at least 50 percent of victory for hiders
    REQUIRE(results[Role::HIDER] / (double)Params::nGames > 0.5);
}

TEST_CASE("check seeker victory", "IAV1seeker vs random")
{
    // Need to register expected class
    HiderFactory::registerClass<RandomHider>("random");
    SeekerFactory::registerClass<IAV1Seeker>("random");
    MazeFactory::registerClass<RandomMaze_v1>("random");

    std::map<Role, unsigned int> results;
    results[Role::HIDER] = 0;
    results[Role::SEEKER] = 0;

    for (int i = 0; i < Params::nGames; i++) {

        // create Game and init maze (required call build)
        auto maze = MazeFactory::create(Params::mazeClassName, GameParams::width, GameParams::height);
        maze->build();

        Game game(maze, GameParams::nRounds);

        // then add Players (hiders and seekers)
        for (unsigned int j = 0; j < GameParams::nHiders; j++) {

            std::string hider_name = "H" + std::to_string(j + 1);
            // only need the player representation server side
            std::shared_ptr<Player> hider = HiderFactory::create(Params::hiderClassName, hider_name);
            game.addPlayer(hider);
        }

        for (unsigned int j = 0; j < GameParams::nSeekers; j++) {

            std::string seeker_name = "S" + std::to_string(j + 1);
            // only need the player representation server side
            std::shared_ptr<Player> seeker = SeekerFactory::create(Params::seekerClassName, seeker_name);
            game.addPlayer(seeker);
        }

        std::cout << "Game n°" << (i + 1) << " with: "
                  << "{hider: " << Params::hiderClassName
                  << ", seeker: " << Params::seekerClassName
                  << ", maze: " << Params::mazeClassName
                  << "}" << std::endl;

        // before running
        REQUIRE(Helper::isValid(game));
        //if (!Helper::isValid(game)) {
        //    std::cout << "Game is not valid: unexpected maze built..." << std::endl;
        //}

        // run the game
        while (!game.end()) {

            auto player = game.getCurrentPlayer();

            // state is composed of <Observation, vector<PlayerAction>>
            auto state = game.getState(player);
            // get the chosen action by IA
            PlayerAction chosenAction = player->play(state.first, state.second);
            // Play the action in the game and retrieve the new game state
            auto observation = game.step(player, chosenAction);
            // updtae the IA
            player->update(state.first, observation, chosenAction);
        }

        Role winner = game.getCurrentWinner();
        std::cout << "-- Winner is: " << roleToString(winner) << " team in "
                  << game.getNumberOfRounds() << " rounds" << std::endl;

        // update stat
        results[winner] += 1;
    }

    std::cout << "--------------------------------------------" << std::endl;
    std::cout << "End of simulation... Results over " << Params::nGames << " games are: " << std::endl;
    for (auto &k : results) {
        std::cout << " -- " << roleToString(k.first) << ": " << k.second << " wins ("
                  << (k.second / (double)Params::nGames) * 100. << "%)" <<  std::endl;
    }

    // at least 50 percent of victory for hiders
    REQUIRE(results[Role::SEEKER] / (double)Params::nGames > 0.5);
}

// - 80 percent of victory vs random with your seeker and/or hider

TEST_CASE("check hiderV2 victory", "IAV2Hider vs random")
{
    // Need to register expected class
    HiderFactory::registerClass<IAHiderV2>("random");
    SeekerFactory::registerClass<RandomSeeker>("random");
    MazeFactory::registerClass<RandomMaze_v3>("random");

    std::map<Role, unsigned int> results;
    results[Role::HIDER] = 0;
    results[Role::SEEKER] = 0;

    for (int i = 0; i < Params::nGames; i++) {

        // create Game and init maze (required call build)
        auto maze = MazeFactory::create(Params::mazeClassName, GameParams::width, GameParams::height);
        maze->build();

        Game game(maze, GameParams::nRounds);

        // then add Players (hiders and seekers)
        for (unsigned int j = 0; j < GameParams::nHiders; j++) {

            std::string hider_name = "H" + std::to_string(j + 1);
            // only need the player representation server side
            std::shared_ptr<Player> hider = HiderFactory::create(Params::hiderClassName, hider_name);
            game.addPlayer(hider);
        }

        for (unsigned int j = 0; j < GameParams::nSeekers; j++) {

            std::string seeker_name = "S" + std::to_string(j + 1);
            // only need the player representation server side
            std::shared_ptr<Player> seeker = SeekerFactory::create(Params::seekerClassName, seeker_name);
            game.addPlayer(seeker);
        }

        std::cout << "Game n°" << (i + 1) << " with: "
                  << "{hider: " << Params::hiderClassName
                  << ", seeker: " << Params::seekerClassName
                  << ", maze: " << Params::mazeClassName
                  << "}" << std::endl;

        // before running
        REQUIRE(Helper::isValid(game));
        //if (!Helper::isValid(game)) {
        //    std::cout << "Game is not valid: unexpected maze built..." << std::endl;
        //}

        // run the game
        while (!game.end()) {

            auto player = game.getCurrentPlayer();

            // state is composed of <Observation, vector<PlayerAction>>
            auto state = game.getState(player);
            // get the chosen action by IA
            PlayerAction chosenAction = player->play(state.first, state.second);
            // Play the action in the game and retrieve the new game state
            auto observation = game.step(player, chosenAction);
            // updtae the IA
            player->update(state.first, observation, chosenAction);
        }

        Role winner = game.getCurrentWinner();
        std::cout << "-- Winner is: " << roleToString(winner) << " team in "
                  << game.getNumberOfRounds() << " rounds" << std::endl;

        // update stat
        results[winner] += 1;
    }

    std::cout << "--------------------------------------------" << std::endl;
    std::cout << "End of simulation... Results over " << Params::nGames << " games are: " << std::endl;
    for (auto &k : results) {
        std::cout << " -- " << roleToString(k.first) << ": " << k.second << " wins ("
                  << (k.second / (double)Params::nGames) * 100. << "%)" <<  std::endl;
    }

    // at least 80 percent of victory for hiders
    REQUIRE(results[Role::HIDER] / (double)Params::nGames > 0.8);
}


TEST_CASE("check IASeekerV2 victory", "IAV2seeker vs random")
{
    // Need to register expected class
    HiderFactory::registerClass<RandomHider>("random");
    SeekerFactory::registerClass<IAV2Seeker>("random");
    MazeFactory::registerClass<RandomMaze_v3>("random");

    std::map<Role, unsigned int> results;
    results[Role::HIDER] = 0;
    results[Role::SEEKER] = 0;

    for (int i = 0; i < Params::nGames; i++) {

        // create Game and init maze (required call build)
        auto maze = MazeFactory::create(Params::mazeClassName, GameParams::width, GameParams::height);
        maze->build();

        Game game(maze, GameParams::nRounds);

        // then add Players (hiders and seekers)
        for (unsigned int j = 0; j < GameParams::nHiders; j++) {

            std::string hider_name = "H" + std::to_string(j + 1);
            // only need the player representation server side
            std::shared_ptr<Player> hider = HiderFactory::create(Params::hiderClassName, hider_name);
            game.addPlayer(hider);
        }

        for (unsigned int j = 0; j < GameParams::nSeekers; j++) {

            std::string seeker_name = "S" + std::to_string(j + 1);
            // only need the player representation server side
            std::shared_ptr<Player> seeker = SeekerFactory::create(Params::seekerClassName, seeker_name);
            game.addPlayer(seeker);
        }

        std::cout << "Game n°" << (i + 1) << " with: "
                  << "{hider: " << Params::hiderClassName
                  << ", seeker: " << Params::seekerClassName
                  << ", maze: " << Params::mazeClassName
                  << "}" << std::endl;

        // before running
        REQUIRE(Helper::isValid(game));
        //if (!Helper::isValid(game)) {
        //    std::cout << "Game is not valid: unexpected maze built..." << std::endl;
        //}

        // run the game
        while (!game.end()) {

            auto player = game.getCurrentPlayer();

            // state is composed of <Observation, vector<PlayerAction>>
            auto state = game.getState(player);
            // get the chosen action by IA
            PlayerAction chosenAction = player->play(state.first, state.second);
            // Play the action in the game and retrieve the new game state
            auto observation = game.step(player, chosenAction);
            // updtae the IA
            player->update(state.first, observation, chosenAction);
        }

        Role winner = game.getCurrentWinner();
        std::cout << "-- Winner is: " << roleToString(winner) << " team in "
                  << game.getNumberOfRounds() << " rounds" << std::endl;

        // update stat
        results[winner] += 1;
    }

    std::cout << "--------------------------------------------" << std::endl;
    std::cout << "End of simulation... Results over " << Params::nGames << " games are: " << std::endl;
    for (auto &k : results) {
        std::cout << " -- " << roleToString(k.first) << ": " << k.second << " wins ("
                  << (k.second / (double)Params::nGames) * 100. << "%)" <<  std::endl;
    }

    // at least 80 percent of victory for seekers
    REQUIRE(results[Role::SEEKER] / (double)Params::nGames > 0.8);
}

// - 100 percent?
