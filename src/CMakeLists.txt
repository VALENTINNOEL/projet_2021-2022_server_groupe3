include_directories(${CMAKE_SOURCE_DIR}/src)

link_directories()

#find_package(PkgConfig)
#pkg_check_modules(restbed REQUIRED IMPORTED_TARGET restbed)

file(GLOB SOURCE_FILES
    player/*.hpp player/*.cpp
    player/hider/*.hpp player/hider/*.cpp
    player/seeker/*.hpp player/seeker/*.cpp
    maze/*.hpp maze/*.cpp
    maze/block/*.hpp maze/block/*.cpp
    maze/block/material/*.hpp maze/block/material/*.cpp
    maze/custom/*.hpp maze/custom/*.cpp
    factory/*.hpp factory/*.cpp
    utils/*.hpp utils/*.cpp
)

# we default to Release build type
if(NOT CMAKE_BUILD_TYPE)
  set(CMAKE_BUILD_TYPE "Release")
endif()

if(CMAKE_CXX_COMPILER_ID MATCHES Intel)
    set(CMAKE_CXX_FLAGS         "${CMAKE_CXX_FLAGS} -Wall")
    set(CMAKE_CXX_FLAGS_DEBUG   "-g -traceback")
    set(CMAKE_CXX_FLAGS_RELEASE "-O3 -ip -xHOST")
endif()

if(CMAKE_CXX_COMPILER_ID MATCHES GNU)
    set(CMAKE_CXX_FLAGS         "${CMAKE_CXX_FLAGS} -Wall")
    set(CMAKE_CXX_FLAGS_DEBUG   "-O0 -g3")
    set(CMAKE_CXX_FLAGS_RELEASE "-Ofast -march=native")
endif()

add_library(hide_and_seek rest_server.hpp rest_client.hpp game.hpp game.cpp ${SOURCE_FILES})

target_link_libraries(hide_and_seek)
set_target_properties(hide_and_seek PROPERTIES LINKER_LANGUAGE CXX)

if(CMAKE_BUILD_TYPE)
    string(TOUPPER "${CMAKE_BUILD_TYPE}" _upper_build_type)
    set(BUILD_${_upper_build_type} 1)
endif()

# Check build for fixed seed into random generator
if(BUILD_DEBUG EQUAL 1)
    set_target_properties(hide_and_seek PROPERTIES COMPILE_DEFINITIONS "FIXED_SEED")
endif()

set(CMAKE_CXX_FLAGS_DEBUG_INIT "-Wall")
set(CMAKE_CXX_FLAGS_RELEASE_INIT "-Wall")

add_executable(server server.cpp)
add_executable(client client.cpp)
add_executable(stats stats.cpp)
target_link_libraries(server hide_and_seek pthread)
target_link_libraries(client hide_and_seek pthread)
target_link_libraries(stats hide_and_seek)