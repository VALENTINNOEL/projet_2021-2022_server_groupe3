#include "randomGenerator.hpp"

#include <iomanip>

RandGenerator::RandGenerator() : eng(rd()), uniDouble(DOUBLE_MIN, DOUBLE_MAX) {
    #if defined(FIXED_SEED)
        eng.seed(42);
    #endif
}

/**
 * @brief Get the Singleton Instance object
 * 
 * @return std::shared_ptr<RandGenerator> 
 */
const std::shared_ptr<RandGenerator> &RandGenerator::getInstance()
{
    if (instance == nullptr)
        instance = std::shared_ptr<RandGenerator>(new RandGenerator());

    return instance;
}

/**
 * @brief Get the Probability between 0 and 1
 * 
 * @return double 
 */
double RandGenerator::getProbability() {

    return uniDouble(eng);
}

/**
 * @brief Get a random number bewteen a specific interval [min, max] (max include)
 * 
 * @param min 
 * @param max 
 * @return int 
 */
int RandGenerator::getRandomIntInterval(int min, int max) {

    // get current uniform distribution using min and max
    std::uniform_int_distribution<int> unifDist(min, max);
    return unifDist(eng);
}


/**
 * @brief Get a random number bewteen a specific max value [0, max] (max include)
 * 
 * @param max 
 * @return int 
 */
int RandGenerator::getRandomInt(int max) {

    // get current uniform distribution using min and max
    std::uniform_int_distribution<int> unifDist(0, max);
    return unifDist(eng);
}

// static member definition
std::shared_ptr<RandGenerator> RandGenerator::instance;
