#ifndef HIDE_AND_SEEK_RANDOM_MAZE_V3_HPP
#define HIDE_AND_SEEK_RANDOM_MAZE_V3_HPP

#include "maze/maze.hpp"
#include "game.hpp"

class RandomMaze_v3 : public Maze
{

public:
    RandomMaze_v3(unsigned int width, unsigned int height) : Maze(width, height) {};

    virtual void build();

    ~RandomMaze_v3() {};
};

#endif