#include "maze/custom/random_v1.hpp"

#include "maze/block/block.hpp"

// specific blocks
#include "maze/block/border.hpp"
#include "maze/block/ground.hpp"
#include "maze/block/material/custom.hpp"
#include "utils/randomGenerator.hpp"

void RandomMaze_v1::build() {

    // Retrieve the current random generator
    std::shared_ptr<RandGenerator> rGenerator = RandGenerator::getInstance();

    double limCases = (this->width) * (this->height);
    double limBlock = limCases * GameParams::pctBlock;
    double limWood = limCases * GameParams::pctWood;
    double limStone = limCases * GameParams::pctStone;
    double limMetal = limCases * GameParams::pctMetal;
    double limFix = limBlock - (limWood + limStone + limMetal + this->width + this->height + (this->width - 2) + (this->height - 2));

    double nBlock = 0;
    double nWood = 0;
    double nStone = 0;
    double nMetal = 0;
    double nFix = 0;

    // build the current Maze
    for (unsigned int i = 0; i < this->height; i++) {
        for (unsigned int j = 0; j < this->width; j++) {

            std::shared_ptr<Block> block;

            // add border
            if (i == 0 || j == 0 || i == (this->height - 1) || j == (this->width - 1)) {
                block = std::make_shared<Border>();
                nBlock ++;
            } else {

                double p = rGenerator->getProbability();

                if(p < GameParams::pctBlock){


                    if(p < GameParams::pctMetal/GameParams::pctBlock && nMetal < limMetal){
                        block = std::make_shared<Metal>();
                        nMetal ++;
                    }

                    else if(p < GameParams::pctStone/GameParams::pctBlock && nStone < limStone){
                        block = std::make_shared<Stone>();
                        nStone ++;
                    }

                    else if (p < GameParams::pctWood/GameParams::pctBlock && nWood < limWood) {
                        block = std::make_shared<Wood>();
                        nWood ++;
                    }

                    else if(nFix < limFix){
                        unsigned int curCell = j + i * height;

                        if(cells[curCell - width]->getBlock()->getKind() != BORDER
                        && cells[curCell - 1]->getBlock()->getKind() != BORDER
                        && cells[curCell - width - 1]->getBlock()->getKind() != BORDER
                        && cells[curCell - width + 1]->getBlock()->getKind() != BORDER){
                            block = std::make_shared<Border>();
                            nFix++;
                        }else{
                            block = std::make_shared<Ground>();
                        }
                    }
                    else{
                        block = std::make_shared<Ground>();
                    }
                }
                else{
                    block = std::make_shared<Ground>();
                }
            }

            // construct new shared ptr Cell with only block
            std::shared_ptr<MazeCell> cell = std::make_shared<MazeCell>(block);
            cells.push_back(cell);
        }
    }
}

