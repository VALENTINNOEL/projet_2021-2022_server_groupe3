#include "maze/custom/random_v2.hpp"

#include "maze/block/block.hpp"

// specific blocks
#include "maze/block/border.hpp"
#include "maze/block/ground.hpp"
#include "maze/block/material/custom.hpp"
#include "utils/randomGenerator.hpp"


void RandomMaze_v2::build() {

    // Retrieve the current random generator
    std::shared_ptr<RandGenerator> rGenerator = RandGenerator::getInstance();
    double limCases = (this->width) * (this->height);
    double limBlock = limCases * GameParams::pctBlock;
    double limWood = limCases * GameParams::pctWood;
    double limStone = limCases * GameParams::pctStone;
    double limMetal = limCases * GameParams::pctMetal;
    double limFix = limBlock - (limWood + limStone + limMetal + this->width + this->height + (this->width - 2) + (this->height - 2));

    double nBlock = 0;
    double nWood = 0;
    double nStone = 0;
    double nMetal = 0;
    double nFix = 0;

    /****************************** ON PLACE LES BORDURES ET ON REMPLIT LE LABYRINTHE DE BLOCS CASSABLES ***************************/

    // build the current Maze
    for (unsigned int i = 0; i < this->height; i++) {
        for (unsigned int j = 0; j < this->width; j++) {

            std::shared_ptr<Block> block;

            // add border
            if (i == 0 || j == 0 || i == (this->height - 1) || j == (this->width - 1)) {
                block = std::make_shared<Border>();
            } else {

                double p = rGenerator->getProbability();

                if(p < (GameParams::pctMetal / GameParams::pctWood)){
                    block = std::make_shared<Metal>();
                    nMetal++;
                    nBlock++;
                }

                else if(p < (GameParams::pctStone / GameParams::pctWood)){
                    block = std::make_shared<Stone>();
                    nStone++;
                    nBlock++;
                }
                else{
                    block = std::make_shared<Wood>();
                    nWood++;
                    nBlock++;
                }
            }

            // construct new shared ptr Cell with only block
            std::shared_ptr<MazeCell> cell = std::make_shared<MazeCell>(block);
            cells.push_back(cell);
        }
    }

    /*************************** ON PLACE ALEATOIREMENT LES BLOCS FIXES  ***********************************/

    while(nFix < limFix){

        unsigned int x = rGenerator->getRandomIntInterval(2, (int)this->width - 3);
        unsigned int y = rGenerator->getRandomIntInterval(2, (int)this->height - 3);

        if(getCell(x, y)->getBlock()->getKind()!= BKind::BORDER){

            if(getCell(x, y - 1)->getBlock()->getKind() != BORDER // HAUT
               && getCell(x, y + 1)->getBlock()->getKind() != BORDER // BAS
               && getCell(x - 1, y)->getBlock()->getKind() != BORDER // GAUCHE
               && getCell(x + 1, y)->getBlock()->getKind() != BORDER // DROITE
               && getCell(x - 1, y - 1)->getBlock()->getKind() != BORDER // DIAGONALE HAUT GAUCHE
               && getCell(x + 1, y - 1)->getBlock()->getKind() != BORDER // DIAGONALE HAUT DROITE
               && getCell(x - 1, y + 1)->getBlock()->getKind() != BORDER // DIAGONALE BAS GAUCHE
               && getCell(x + 1, y + 1)->getBlock()->getKind() != BORDER){ // DIAGONALE BAS DROITE

                auto material = std::dynamic_pointer_cast<Material>(getCell(x, y)->getBlock());

                if(material->getMaterialKind() == MaterialKind::METAL && nMetal > limMetal){ // Si le bloc remplacé est un METAL

                    std::shared_ptr<Block> block;
                    block = std::make_shared<Border>();
                    getCell(x ,y)->setBlock(block);
                    nMetal--;
                    nFix++;
                }

                else if(material->getMaterialKind() == MaterialKind::STONE && nStone > limStone){ // Si le bloc remplacé est un STONE

                    std::shared_ptr<Block> block;
                    block = std::make_shared<Border>();
                    getCell(x ,y)->setBlock(block);
                    nStone--;
                    nFix++;
                }

                else if(material->getMaterialKind() == MaterialKind::WOOD && nWood > limWood){ // Si le bloc remplacé est un WOOD

                    std::shared_ptr<Block> block;
                    block = std::make_shared<Border>();
                    getCell(x ,y)->setBlock(block);
                    nWood--;
                    nFix++;
                }
            }
        }
    }

    /*************************** ON CREUSE DES CHEMINS DANS LE LABYRINTHE *********************************/

    std::shared_ptr<Block> block;
    block = std::make_shared<Ground>();

    auto pos_1 = rGenerator->getRandomIntInterval(1, (int)((this->width - 2) *0.3)) + this->width;  // ON PLACE UN "BOT" EN BORDURE HAUTE
    auto pos_2 = rGenerator->getRandomIntInterval(1, (int)((this->width - 2) *0.3)) + this->width * (this->height - 2);  // ON PLACE UN "BOT" EN BORDURE BASSE
    auto pos_3 = rGenerator->getRandomIntInterval(1, (int)((this->height - 2)*0.3)) * this->width + 1;  // ON PLACE UN "BOT" EN BORDURE DROITE
    auto pos_4 = rGenerator->getRandomIntInterval(1, (int)((this->width - 2) *0.3)) * this->width + this->width - 2;  // ON PLACE UN "BOT" EN BORDURE GAUCHE

    auto pos_5 = rGenerator->getRandomIntInterval((int)((this->width - 2)  *0.3), (int)((this->width - 2)  *0.3)) + this->width;  // ON PLACE UN "BOT" EN BORDURE HAUTE
    auto pos_6 = rGenerator->getRandomIntInterval((int)((this->width - 2)  *0.3), (int)((this->width - 2)  *0.3)) + this->width * (this->height - 2);  // ON PLACE UN "BOT" EN BORDURE BASSE
    auto pos_7 = rGenerator->getRandomIntInterval((int)((this->height - 2) *0.3), (int)((this->height - 2) *0.6)) * this->width + 1;  // ON PLACE UN "BOT" EN BORDURE DROITE
    auto pos_8 = rGenerator->getRandomIntInterval((int)((this->height - 2) *0.3), (int)((this->height - 2) *0.6)) * this->width + this->width - 2;  // ON PLACE UN "BOT" EN BORDURE GAUCHE

    auto pos_9 = rGenerator->getRandomIntInterval((int)((this->width - 2)  *0.6), (int)(this->width - 2)) + this->width;  // ON PLACE UN "BOT" EN BORDURE HAUTE
    auto pos_10 = rGenerator->getRandomIntInterval((int)((this->width - 2)  *0.6), (int)(this->width - 2)) + this->width * (this->height - 2);  // ON PLACE UN "BOT" EN BORDURE BASSE
    auto pos_11 = rGenerator->getRandomIntInterval((int)((this->height - 2) *0.6), (int)(this->height - 2)) * this->width + 1;  // ON PLACE UN "BOT" EN BORDURE DROITE
    auto pos_12 = rGenerator->getRandomIntInterval((int)((this->height - 2) *0.6), (int)(this->height - 2)) * this->width + this->width - 2;  // ON PLACE UN "BOT" EN BORDURE GAUCHE


    auto x = rGenerator->getRandomIntInterval((int)(this->width / 2.5), (int)(this->width / 1.50));
    auto y = rGenerator->getRandomIntInterval((int)(this->width / 2.5), (int)(this->width / 1.50));
    auto pos_13 = x + this->width * y; // ON PLACE UN "BOT" AU CENTRE DE LA CARTE

    while(cells[pos_13]->getBlock()->getKind() == BKind::BORDER){
        x = rGenerator->getRandomIntInterval((int)(this->width / 2.5), (int)(this->width / 1.50));
        y = rGenerator->getRandomIntInterval((int)(this->width / 2.5), (int)(this->width / 1.50));

        if(getCell(x, y)->getBlock()->getKind() != BKind::BORDER){
            pos_13 = x + this->width * y;
        }
    }

    unsigned int position[13] = {pos_1, pos_2, pos_3, pos_4, pos_5, pos_6, pos_7, pos_8, pos_9, pos_10, pos_11, pos_12, pos_13};
    double nBlock_destr = nBlock - limWood - limStone - limMetal;

    for (auto pos : position){ // Les "bots" creusent chacun leur tour le labyrinthe

        if (cells[pos]->getBlock()->getKind() == BKind::MATERIAL){

            auto material = std::dynamic_pointer_cast<Material>(cells[pos]->getBlock());

            if(material->getMaterialKind() == MaterialKind::METAL) nMetal--;
            else if(material->getMaterialKind() == MaterialKind::STONE) nStone--;
            else nWood--;
        }

        cells[pos]->setBlock(block);
        bool exit = false;
        unsigned int cpt = 0;

        while(cpt < (nBlock_destr / 6) && !exit){

            unsigned int p = rGenerator->getRandomIntInterval(0, 3); // Les bots choisissent une direction

            if (p == 0){ // BAS

                if (cells[pos + this->width]->getBlock()->getKind() == BKind::MATERIAL){ // Si le bloc du dessous est un MATERIAL

                    auto material = std::dynamic_pointer_cast<Material>(cells[pos + this->width]->getBlock());

                    if(material->getMaterialKind() == MaterialKind::WOOD && nWood > limWood){ // Si on trouve WOOD et que sa limite n'est pas respectée
                        pos = pos + this->width;
                        cells[pos]->setBlock(block);
                        cpt ++;
                        nWood --;
                    }

                    if(material->getMaterialKind() == MaterialKind::STONE && nStone > limStone){ // Si on trouve STONE et que sa limite n'est pas respectée
                        pos = pos + this->width;
                        cells[pos]->setBlock(block);
                        cpt ++;
                        nStone --;
                    }

                    if(material->getMaterialKind() == MaterialKind::METAL && nMetal > limMetal){ // Si on trouve METAL et que sa limite n'est pas respectée
                        pos = pos + this->width;
                        cells[pos]->setBlock(block);
                        cpt ++;
                        nMetal --;
                    }

                }
            }

            if (p == 1){ // GAUCHE

                if (cells[pos - 1]->getBlock()->getKind() == BKind::MATERIAL){ // Si le bloc de gauche est un MATERIAL

                    auto material = std::dynamic_pointer_cast<Material>(cells[pos - 1]->getBlock());

                    if(material->getMaterialKind() == MaterialKind::WOOD && nWood > limWood){ // Si on trouve WOOD et que sa limite n'est pas respectée
                        pos = pos - 1;
                        cells[pos]->setBlock(block);
                        cpt ++;
                        nWood --;
                    }

                    if(material->getMaterialKind() == MaterialKind::STONE && nStone > limStone){ // Si on trouve STONE et que sa limite n'est pas respectée
                        pos = pos - 1;
                        cells[pos]->setBlock(block);
                        cpt ++;
                        nStone --;
                    }

                    if(material->getMaterialKind() == MaterialKind::METAL && nMetal > limMetal){ // Si on trouve METAL et que sa limite n'est pas respectée
                        pos = pos - 1;
                        cells[pos]->setBlock(block);
                        cpt ++;
                        nMetal --;
                    }
                }
            }

            if (p == 2){ // DROITE

                if(cells[pos + 1]->getBlock()->getKind() == BKind::MATERIAL){ // Si le bloc de droite est un MATERIAL

                    auto material = std::dynamic_pointer_cast<Material>(cells[pos + 1]->getBlock());

                    if(material->getMaterialKind() == MaterialKind::WOOD && nWood > limWood){ // Si on trouve WOOD et que sa limite n'est pas respectée
                        pos = pos + 1;
                        cells[pos]->setBlock(block);
                        cpt ++;
                        nWood --;
                    }

                    if(material->getMaterialKind() == MaterialKind::STONE && nStone > limStone){ // Si on trouve STONE et que sa limite n'est pas respectée
                        pos = pos + 1;
                        cells[pos]->setBlock(block);
                        cpt ++;
                        nStone --;
                    }

                    if(material->getMaterialKind() == MaterialKind::METAL && nMetal > limMetal){ // Si on trouve METAL et que sa limite n'est pas respectée
                        pos = pos + 1;
                        cells[pos]->setBlock(block);
                        cpt ++;
                        nMetal --;
                    }
                }
            }

            if (p == 3){ // HAUT

                if (cells[pos - this->width]->getBlock()->getKind() == BKind::MATERIAL){ // Si le bloc du haut est un MATERIAL

                    auto material = std::dynamic_pointer_cast<Material>(cells[pos - this->width]->getBlock());

                    if(material->getMaterialKind() == MaterialKind::WOOD && nWood > limWood){ // Si on trouve WOOD et que sa limite n'est pas respectée
                        pos = pos - this->width;
                        cells[pos]->setBlock(block);
                        cpt ++;
                        nWood --;
                    }

                    if(material->getMaterialKind() == MaterialKind::STONE && nStone > limStone){ // Si on trouve STONE et que sa limite n'est pas respectée
                        pos = pos - this->width;
                        cells[pos]->setBlock(block);
                        cpt ++;
                        nStone --;
                    }

                    if(material->getMaterialKind() == MaterialKind::METAL && nMetal > limMetal){ // Si on trouve METAL et que sa limite n'est pas respectée
                        pos = pos - this->width;
                        cells[pos]->setBlock(block);
                        cpt ++;
                        nMetal --;
                    }
                }
            }

            if (((cells[pos + this->width]->getBlock()->getKind() != BKind::MATERIAL)
                  && (cells[pos - 1]->getBlock()->getKind() != BKind::MATERIAL)
                  && (cells[pos + 1]->getBlock()->getKind() != BKind::MATERIAL)
                  && (cells[pos - this->width]->getBlock()->getKind() != BKind::MATERIAL))
                  || (nWood == limWood || nStone == limStone || nMetal == limMetal) ){
                exit = true;
            }
        }
    }
/********************************* ON RETIRE LES BLOCS EN TROP  ******************************************/

    while(nMetal > limMetal || nStone > limStone || nWood > limWood){ // TANT QU'IL Y A DES BLOCS EN TROP

        x = rGenerator->getRandomIntInterval(1, (int)this->width - 2);
        y = rGenerator->getRandomIntInterval(1, (int)this->height - 2);

        if (getCell(x, y)->getBlock()->getKind() == BKind::MATERIAL){ // Si on trouve un MATERIAL

            auto material = std::dynamic_pointer_cast<Material>(getCell(x, y)->getBlock());

            if (material->getMaterialKind() == MaterialKind::METAL && nMetal > limMetal){ // Si on trouve un METAL et que sa limite n'est pas respectée

                getCell(x, y)->setBlock(block);
                nMetal --;
            }

            if (material->getMaterialKind() == MaterialKind::STONE && nStone > limStone){ // Si on trouve un STONE et que sa limite n'est pas respectée

                getCell(x, y)->setBlock(block);
                nStone --;
            }

            if (material->getMaterialKind() == MaterialKind::WOOD && nWood > limWood){ // Si on trouve un WOOD et que sa limite n'est pas respectée

                getCell(x, y)->setBlock(block);
                nWood --;
            }
        }
    }
}