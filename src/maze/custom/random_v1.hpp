#ifndef HIDE_AND_SEEK_RANDOM_MAZE_V1_HPP
#define HIDE_AND_SEEK_RANDOM_MAZE_V1_HPP

#include "maze/maze.hpp"
#include "game.hpp"

class RandomMaze_v1 : public Maze
{

public:
    RandomMaze_v1(unsigned int width, unsigned int height) : Maze(width, height) {};

    virtual void build();

    ~RandomMaze_v1() {};
};

#endif