#include "maze/custom/random_v3.hpp"
#include "maze/block/block.hpp"

// specific blocks
#include "maze/block/border.hpp"
#include "maze/block/ground.hpp"
#include "maze/block/material/custom.hpp"
#include "utils/randomGenerator.hpp"


void RandomMaze_v3::build() {

    // Retrieve the current random generator
    std::shared_ptr<RandGenerator> rGenerator = RandGenerator::getInstance();

    double limCases = (this->width) * (this->height);
    double limBlock = limCases * GameParams::pctBlock;
    double limWood = limCases * GameParams::pctWood;
    double limStone = limCases * GameParams::pctStone;
    double limMetal = limCases * GameParams::pctMetal;
    double limFix = limBlock - (limWood + limStone + limMetal + this->width + this->height + (this->width - 2) + (this->height - 2));

    double nWood = 0;
    double nStone = 0;
    double nMetal = 0;
    double nFix = 0;

    /******************* ON PLACE LES BORDURES ET LE SOL *******************/

    for (unsigned int i = 0; i < this->height; i++) {
        for (unsigned int j = 0; j < this->width; j++) {

            std::shared_ptr<Block> block;

            if (i == 0 || j == 0 || i == (this->height - 1) || j == (this->width - 1)) { // On ajoute les bordures de carte
                block = std::make_shared<Border>();
            }

            else { // On ajoute le sol
                block = std::make_shared<Ground>();
            }

            // On stocke les blocs dans un vecteur
            std::shared_ptr<MazeCell> cell = std::make_shared<MazeCell>(block);
            cells.push_back(cell);
        }
    }

    /******************** ON PLACE LES BLOCS FIXES ************************/

    while(nFix != limFix){ //Tant que la limite de blocs fixes n'est pas respectée

        auto x = rGenerator->getRandomIntInterval(1, (int)this->width - 2);
        auto y = rGenerator->getRandomIntInterval(1, (int)this->height - 2);

        //On vérifie qu'aucun bloc fixe ne soit collé avec un autre
        if( (x%2 || y%2)
            && getCell(x, y)->getBlock()->getKind() != BORDER
            && getCell(x + 1, y)->getBlock()->getKind() != BORDER
            && getCell(x - 1, y)->getBlock()->getKind() != BORDER
            && getCell(x, y + 1)->getBlock()->getKind() != BORDER
            && getCell(x, y - 1)->getBlock()->getKind() != BORDER
            && getCell(x + 1, y + 1)->getBlock()->getKind() != BORDER
            && getCell(x - 1, y + 1)->getBlock()->getKind() != BORDER
            && getCell(x + 1, y - 1)->getBlock()->getKind() != BORDER
            && getCell(x - 1, y - 1)->getBlock()->getKind() != BORDER){

            std::shared_ptr<Block> block;
            block = std::make_shared<Border>();
            getCell(x, y)->setBlock(block);
            nFix++;
        }
    }

    /************************ ON PLACE LES MATERIAUX ***********************/

    for (unsigned int i = 1; i < this->height - 1; i++) {
        for (unsigned int j = 1; j < this->width - 1; j++) {

            if( (i%2 || j%2) && getCell(j, i)->getBlock()->getKind() != BKind::BORDER){

                std::shared_ptr<Block> block;
                auto p = rGenerator->getProbability();

                if(p < GameParams::pctMetal / GameParams::pctWood){
                    block = std::make_shared<Metal>();
                    getCell(j,i)->setBlock(block);
                    nMetal++;
                }

                else if(p < GameParams::pctStone / GameParams::pctWood){
                    block = std::make_shared<Stone>();
                    getCell(j,i)->setBlock(block);
                    nStone++;
                }

                else{
                    block = std::make_shared<Wood>();
                    getCell(j,i)->setBlock(block);
                    nWood++;
                }
            }

        }
    }

    /******* ON CREE LE TABLEAU QUI CONTIENT LES VALEURS DES CASES DU LABYRINTHE  ******/

    int tab [this->height][this->width];
    int cpt = 0;

    for (unsigned int i = 0; i < this->height; i++) {
        for (unsigned int j = 0; j < this->width; j++) {

            if(getCell(j, i)->getBlock()->getKind() == BKind::GROUND){
                tab[j][i] = cpt;
                cpt++;
            }
            else{
                tab[j][i] = -1;
            }
        }
    }

    /**************************** ON CREUSE LE LABYRINTHE *****************************/

    bool quit = false;

    while((nMetal > limMetal || nStone > limStone || nWood > limWood) && !quit){

        auto x = rGenerator->getRandomIntInterval(1, (int)this->width - 2);
        auto y = rGenerator->getRandomIntInterval(1, (int)this->height - 2);

        if (!(x%2 || y%2)){

            std::shared_ptr<Block> block;
            auto p = rGenerator->getRandomIntInterval(0, 3);

            if(p == 0){ /******** BAS ********/

                if(getCell(x, y+1)->getBlock()->getKind() == BKind::MATERIAL){ // Si le bloc du bas est un MATERIAL
                    if(tab[x][y+2] != tab[x][y] && tab[x][y+2] != -1){ // Si le bloc vide du bas n'est pas visité

                        auto material = std::dynamic_pointer_cast<Material>(getCell(x, y+1)->getBlock());

                        // Si le bloc du bas peut être remplacé
                        if((material->getMaterialKind() == MaterialKind::METAL && nMetal > limMetal)
                           || (material->getMaterialKind() == MaterialKind::STONE && nStone > limStone)
                           || (material->getMaterialKind() == MaterialKind::WOOD && nWood > limWood)){


                            if(material->getMaterialKind() == MaterialKind::METAL && nMetal > limMetal){ // Si c'est un METAL
                                nMetal--;
                            }
                            else if(material->getMaterialKind() == MaterialKind::STONE && nStone > limStone){ // Si c'est un STONE
                                nStone--;
                            }
                            else{ // Si c'est un WOOD
                                nWood--;
                            }

                            block = std::make_shared<Ground>();
                            getCell(x, y+1)->setBlock(block); // On remplace le MATERIAL par du sol

                            // On met à jour le tableau
                            int valCell1 = tab[x][y];
                            int valCell2 = tab[x][y+2];

                            tab[x][y+1] = valCell1;

                            for (unsigned int i = 1; i < this->height-1; i++) {
                                for (unsigned int j = 1; j < this->width-1; j++) {

                                    if(tab[j][i] == valCell2) tab[j][i] = valCell1;
                                }
                            }
                        }
                    }
                }
            }

            if(p == 1){ /******** GAUCHE ********/

                if(getCell(x-1, y)->getBlock()->getKind() == BKind::MATERIAL){ // Si le bloc de gauche est un MATERIAL
                    if(tab[x-1][y] != tab[x][y] && tab[x-2][y] != -1){ // Si le bloc vide de gauche n'est pas visité

                        auto material = std::dynamic_pointer_cast<Material>(getCell(x-1, y)->getBlock());

                        // Si le bloc de gauche peut être remplacé
                        if((material->getMaterialKind() == MaterialKind::METAL && nMetal > limMetal)
                           || (material->getMaterialKind() == MaterialKind::STONE && nMetal > limMetal)
                           || (material->getMaterialKind() == MaterialKind::WOOD && nMetal > limMetal)){


                            if(material->getMaterialKind() == MaterialKind::METAL && nMetal > limMetal){ // Si c'est un METAL
                                nMetal--;
                            }
                            else if(material->getMaterialKind() == MaterialKind::STONE && nStone > limStone){ // Si c'est un STONE
                                nStone--;
                            }
                            else{ // Si c'est un WOOD
                                nWood--;
                            }



                            block = std::make_shared<Ground>();
                            getCell(x-1, y)->setBlock(block); // On remplace le MATERIAL par du sol

                            // On met à jour le tableau
                            int valCell1 = tab[x][y];
                            int valCell2 = tab[x-2][y];

                            tab[x-1][y] = valCell1;

                            for (unsigned int i = 1; i < this->height-1; i++) {
                                for (unsigned int j = 1; j < this->width-1; j++) {

                                    if(tab[j][i] == valCell2) tab[j][i] = valCell1;
                                }
                            }
                        }

                    }
                }
            }

            if(p == 2){ /******** DROITE ********/

                if(getCell(x+1, y)->getBlock()->getKind() == BKind::MATERIAL) // Si le bloc de droite est un MATERIAL
                    if(tab[x+1][y] != tab[x][y] && tab[x][y+2] != -1){ // Si le bloc vide de droite n'est pas visité

                        auto material = std::dynamic_pointer_cast<Material>(getCell(x+1, y)->getBlock());

                        // Si le bloc de droite peut être remplacé
                        if((material->getMaterialKind() == MaterialKind::METAL && nMetal > limMetal)
                           || (material->getMaterialKind() == MaterialKind::STONE && nMetal > limMetal)
                           || (material->getMaterialKind() == MaterialKind::WOOD && nMetal > limMetal)){


                            if(material->getMaterialKind() == MaterialKind::METAL && nMetal > limMetal){ // Si c'est un METAL
                                nMetal--;
                            }
                            else if(material->getMaterialKind() == MaterialKind::STONE && nStone > limStone){ // Si c'est un STONE
                                nStone--;
                            }
                            else{ // Si c'est un WOOD
                                nWood--;
                            }

                            block = std::make_shared<Ground>();
                            getCell(x+1, y)->setBlock(block); // On remplace le MATERIAL par du sol

                            // On met à jour le tableau
                            int valCell1 = tab[x][y];
                            int valCell2 = tab[x+2][y];

                            tab[x+1][y] = valCell1;

                            for (unsigned int i = 1; i < this->height-1; i++) {
                                for (unsigned int j = 1; j < this->width-1; j++) {

                                    if(tab[j][i] == valCell2) tab[j][i] = valCell1;
                                }
                            }
                        }
                    }
                }

            if(p == 3){ /******** HAUT ********/

                if(getCell(x, y-1)->getBlock()->getKind() == BKind::MATERIAL){ // Si le bloc du haut est un MATERIAL
                    if(tab[x][y-2] != tab[x][y] && tab[x][y-2] != -1){ // Si le bloc vide du haut n'est pas visité

                        auto material = std::dynamic_pointer_cast<Material>(getCell(x, y-1)->getBlock());

                        // Si le bloc du haut peut être remplacé
                        if((material->getMaterialKind() == MaterialKind::METAL && nMetal > limMetal)
                           || (material->getMaterialKind() == MaterialKind::STONE && nStone > limStone)
                           || (material->getMaterialKind() == MaterialKind::WOOD && nWood > limWood)){

                            if(material->getMaterialKind() == MaterialKind::METAL && nMetal > limMetal){ // Si c'est un METAL
                                nMetal--;
                            }
                            else if(material->getMaterialKind() == MaterialKind::STONE && nStone > limStone){ // Si c'est un STONE
                                nStone--;
                            }
                            else{ // Si c'est un WOOD
                                nWood--;
                            }

                            block = std::make_shared<Ground>();
                            getCell(x, y-1)->setBlock(block); // On remplace le MATERIAL par du sol

                            // On met à jour le tableau
                            int valCell1 = tab[x][y];
                            int valCell2 = tab[x][y-2];

                            tab[x][y-1] = valCell1;

                            for (unsigned int i = 1; i < this->height-1; i++) {
                                for (unsigned int j = 1; j < this->width-1; j++) {

                                    if(tab[j][i] == valCell2) tab[j][i] = valCell1;
                                }
                            }
                        }
                    }
                }
            }

    /******************** ON VERIFIE QUE TOUTES LES CASES SOIENT ACCESSIBLES ***********************/

            int valCell = tab[this->width - 2][this->height - 2];
            bool test = false;

            for (unsigned int i = 1; i < this->height - 1; i++) {
                for (unsigned int j = 1; j < this->width - 1; j++) {

                    if(tab[j][i] != valCell && tab[j][i] != -1){
                        test = true;
                    }
                }
            }
            if(!test){ //Si toutes les cases vides n'ont pas la même valeur
                quit = true;
            }
        }
    }

    /*************** ON FINIT DE CASSER DES BLOCS POUR RESPECTER LES LIMITES *******************/

    while(nMetal > limMetal || nStone > limStone || nWood > limWood){

        //On casse d'abord les lignes et colonnes en bordure de carte pour préserver au possible le milieu
        for(unsigned int y = 1; y < this->height - 1 ;y++) {
            for (unsigned int x = 1; x < this->width - 1; x++) {

                if(x == 1 || x == this->width - 2 || y == 1 || y == this->height - 2){ // Si on est sur la ligne ou colonne en bordure de carte

                    if(getCell(x, y)->getBlock()->getKind() == BKind::MATERIAL){

                        std::shared_ptr<Block> block;
                        block = std::make_shared<Ground>();

                        auto material = std::dynamic_pointer_cast<Material>(getCell(x, y)->getBlock());

                        if(material->getMaterialKind() == MaterialKind::METAL && nMetal > limMetal){
                            getCell(x, y)->setBlock(block);
                            nMetal--;
                        }

                        else if(material->getMaterialKind() == MaterialKind::STONE && nStone > limStone){
                            getCell(x, y)->setBlock(block);
                            nStone--;
                        }

                        else if(material->getMaterialKind() == MaterialKind::WOOD && nWood > limWood){
                            getCell(x, y)->setBlock(block);
                            nWood--;
                        }
                    }
                }
            }
        }

        auto x = rGenerator->getRandomIntInterval(2, (int)this->width - 3);
        auto y = rGenerator->getRandomIntInterval(2, (int)this->height - 3);

        if(getCell(x, y)->getBlock()->getKind() == BKind::MATERIAL){ // Si le bloc que l'on veut casser est un MATERIAL

            std::shared_ptr<Block> block;
            block = std::make_shared<Ground>();
            auto material = std::dynamic_pointer_cast<Material>(getCell(x, y)->getBlock());

            if(material->getMaterialKind() == MaterialKind::METAL && nMetal > limMetal){ // Si c'est un METAL
                getCell(x, y)->setBlock(block);
                nMetal--;
            }

            else if(material->getMaterialKind() == MaterialKind::STONE && nStone > limStone){ // Si c'est un STONE
                getCell(x, y)->setBlock(block);
                nStone--;
            }

            else if(material->getMaterialKind() == MaterialKind::WOOD && nWood > limWood){ // Si c'est un WOOD
                getCell(x, y)->setBlock(block);
                nWood--;
            }

        }
    }
}
