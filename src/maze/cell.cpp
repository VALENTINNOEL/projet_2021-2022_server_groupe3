#include "maze/cell.hpp"

#include "player/player.hpp"

nlohmann::json MazeCell::toJSON() const {
    
    nlohmann::json json_data;

    json_data["block"] = block->toJSON();
    json_data["player"] = player->toJSON();

    return json_data;
}

std::shared_ptr<MazeCell> MazeCell::fromJSON(const nlohmann::json &data) {
    
    std::shared_ptr<Block> block = Block::fromJSON(data["block"]);

    return std::make_shared<MazeCell>(block);
}

bool MazeCell::hasPlayer() {
    return player != nullptr;
}
    
const std::shared_ptr<Block> MazeCell::getBlock() const {
    return block;
}

const std::shared_ptr<Player> MazeCell::getPlayer() const {
    return player;
}

void MazeCell::setPlayer(std::shared_ptr<Player> player) {
    this->player = player;
}

void MazeCell::setBlock(std::shared_ptr<Block> block) {
    this->block = block;
}

MazeCell::~MazeCell() {
}