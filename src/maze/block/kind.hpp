#ifndef HIDE_AND_SEEK_BLOCK_KIND_HPP
#define HIDE_AND_SEEK_BLOCK_KIND_HPP

#include <string>

/**
 * @brief Action enum which stores all possible kind of block for a Maze
 * 
 */
enum BKind {
    GROUND, BORDER, MATERIAL, UNDEFINED
};

std::string blockKindToString(enum BKind kind);
std::string blockKindToStringForJSON(enum BKind kind);

#endif