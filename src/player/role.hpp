#ifndef HIDE_AND_SEEK_PLAYER_ROLE_HPP
#define HIDE_AND_SEEK_PLAYER_ROLE_HPP

#include <string>

/**
 * @brief Action enum which stores all possible roles for player
 * 
 */
enum Role {
    SEEKER, HIDER
};

std::string roleToString(enum Role role);

#endif