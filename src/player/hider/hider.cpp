#include "player/hider/hider.hpp"

/**
 * @brief Construct a new Hider:: Hider object
 * 
 * Specify the current hider possible actions
 * 
 * @param name 
 */
Hider::Hider(const std::string &name) : Player(name), block(nullptr), captured(false) {

    // list all the actions for seeker
    for (auto o : PlayerAction::getOrientations()) {
        possibleActions.push_back(PlayerAction(o, Interaction::MOVE));
        possibleActions.push_back(PlayerAction(o, Interaction::CARRY));
        possibleActions.push_back(PlayerAction(o, Interaction::PLACE));
        possibleActions.push_back(PlayerAction(o, Interaction::NOTHING));
    }
}


/**
 * @brief Specific SEEKER role 
 * 
 * @return Role 
 */
Role Hider::getRole() const {
    return Role::HIDER;
}

/**
 * @brief Change captured state of the current hider
 * 
 * @param captured 
 */
void Hider::setCaptured(const bool &captured) {
    this->captured = captured;
}

/**
 * @brief Specify if hider has been captured or not
 * 
 * @return true 
 * @return false 
 */
const bool &Hider::isCaptured() const {
    return captured;
}

/**
 * @brief Get the catch block by Hider
 * 
 * @return const std::shared_ptr<Block> 
 */
const std::shared_ptr<Block> &Hider::getBlock() const {
    return block;
}

/**
 * @brief Set the current block of Hider
 * 
 */
void Hider::setBlock(const std::shared_ptr<Block> &block) {
    this->block = block;
}

const bool Hider::hasBlock() const {
    return block != nullptr;
}

/**
 * @brief Specific json output of the hider
 * 
 * @return nlohmann::json
 */
nlohmann::json Hider::toJSON() const {
    
    // call mother method and add some specification
    nlohmann::json j = Player::toJSON();

    j["captured"] = this->isCaptured();

    if (this->hasBlock()) 
        j["block"] = block->toJSON();
    
    return j;
}

/**
 * @brief Specific display of the player
 * 
 * @return const std::string 
 */
const std::string Hider::toString() const {
    return "{role:" + roleToString(this->getRole()) + ", name: " + this->getName() 
        + ", view: " + orientationToString(orientation) 
        + ", hasBlock:" + std::to_string(this->hasBlock()) + "}";
}

/**
 * @brief Destroy the Hider:: Hider object
 * 
 */
Hider::~Hider() {}