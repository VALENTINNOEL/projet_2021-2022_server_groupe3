#include "player/hider/ia_hider_v1.hpp"
#include "utils/randomGenerator.hpp"

#include <vector>
#include <unistd.h>

/**
 * @brief Construct a new AIHiderV1:: AIHiderV1 object
 * 
 * 
 * @param name 
 */
IAHiderV1::IAHiderV1(const std::string &name) : Hider(name){}


/**
 * @brief Destroy the Hider:: Hider object
 * 
 */
IAHiderV1::~IAHiderV1() {}

/**
 * @brief Return a random action to do
 * 
 * @param state 
 * @return Action 
 */
const PlayerAction &IAHiderV1::play(const std::shared_ptr<Observation> &observation, const std::vector<PlayerAction> &actions) const {

    std::shared_ptr<RandGenerator> rGenerator = RandGenerator::getInstance();

    for(int l = 0; l < std::get<0>(observation->getSize()) ; l++) {
        for (int c = 0; c < std::get<1>(observation->getSize()); c++) {
            if(observation->getCells()[c + l * std::get<0>(observation->getSize())]->hasPlayer()){
                if (observation->getCells()[c + l * std::get<0>(observation->getSize())]->getPlayer()->getRole() == SEEKER){
                    for (auto &act : actions){
                        if (getOrientation() == TOP){
                            if(act.interaction == MOVE && act.orientation == BOTTOM) return act;
                        }else if(getOrientation() == BOTTOM){
                            if(act.interaction == MOVE && act.orientation == TOP) return act;
                        }else if(getOrientation() == RIGHT){
                            if(act.interaction == MOVE && act.orientation == LEFT) return act;
                        }else{
                            if(act.interaction == MOVE && act.orientation == RIGHT) return act;
                        }
                    }
                }
            }
        }
    }

    return actions[rGenerator->getRandomInt(actions.size() - 1)];
}

void IAHiderV1::update(const std::shared_ptr<Observation> &prev, const std::shared_ptr<Observation> &next, const PlayerAction &action) {
    // Do something...
};