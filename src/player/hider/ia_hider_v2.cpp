#include "player/hider/ia_hider_v2.hpp"
#include "utils/randomGenerator.hpp"
#include <cmath>

#include <vector>
#include <unistd.h>

/**
 * @brief Construct a new AIHiderV2:: AIHiderV2 object
 *
 *
 * @param name
 */
IAHiderV2::IAHiderV2(const std::string &name) : Hider(name){
    lastSeekerPos.x = lastSeekerPos.y = 0;
    state = HIDDEN;
}


/**
 * @brief Destroy the Hider:: Hider object
 *
 */
IAHiderV2::~IAHiderV2() {}

/**
 * @brief
 *
 * @param state
 * @return Action
 */
const PlayerAction &IAHiderV2::play(const std::shared_ptr<Observation> &observation, const std::vector<PlayerAction> &actions) const {

    std::shared_ptr<RandGenerator> rGenerator = RandGenerator::getInstance();

    if(!hasBlock()){
        for(auto &act : actions){
            if(act.interaction == CARRY)return act;
        }
    }

    if(state == RUNNING){
        for(int l = 0; l < std::get<0>(observation->getSize()) ; l++) {
            for (int c = 0; c < std::get<1>(observation->getSize()); c++) {
                if(memory[c + l * std::get<0>(observation->getSize())]->hasPlayer()){
                    if (memory[c + l * std::get<0>(observation->getSize())]->getPlayer()->getRole() == SEEKER){
                        Point seekerPos = memory[c + l * std::get<0>(observation->getSize())]->getPlayer()->getLocation();
                        int diffX = getLocation().x - seekerPos.x;
                        int diffY = getLocation().y - seekerPos.y;
                        for (auto &act : actions){
                            if (abs(diffY) < abs(diffX)) {
                                if (diffY > 0) {
                                    if(act.interaction == PLACE && act.orientation == TOP)return act;
                                    else if(act.interaction == MOVE && act.orientation == BOTTOM) return act;
                                } else if (diffY < 0){
                                    if(act.interaction == PLACE && act.orientation == BOTTOM)return act;
                                    else if(act.interaction == MOVE && act.orientation == TOP) return act;
                                }
                            }else{
                                if (diffX > 0){
                                    if(act.interaction == PLACE && act.orientation == LEFT)return act;
                                    else if(act.interaction == MOVE && act.orientation == RIGHT) return act;
                                }else{
                                    if(act.interaction == PLACE && act.orientation == RIGHT)return act;
                                    else if(act.interaction == MOVE && act.orientation == LEFT) return act;
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    auto act = rGenerator->getRandomInt(actions.size() - 1);
    while(actions[act].interaction == PLACE){
        act = rGenerator->getRandomInt(actions.size() - 1);
    }
    return actions[act];
}

void IAHiderV2::update(const std::shared_ptr<Observation> &prev, const std::shared_ptr<Observation> &next, const PlayerAction &action) {
    // Do something...
    memory = next->getCells();

    if (seekerSeen(next)){
        state = RUNNING;
    }else if(seekerSeen(prev) && !seekerSeen(next)){
        memory[lastSeekerPos.x + lastSeekerPos.y * std::get<0>(next->getSize())]->setPlayer(lastSeekerSeen);
        state = RUNNING;
    }else if(!seekerSeen(next) && lastSeekerSeen != nullptr){
        next->getCells()[lastSeekerPos.x + lastSeekerPos.y * std::get<0>(next->getSize())]->setPlayer(lastSeekerSeen);
        state = RUNNING;
    }else{
        state = HIDDEN;
    }
}

bool IAHiderV2::seekerSeen(const std::shared_ptr<Observation> &observation){
    for(int l = 0; l < std::get<0>(observation->getSize()); l++) {
        for (int c = 0; c < std::get<1>(observation->getSize()); c++) {
            if(observation->getCells()[c + l * std::get<0>(observation->getSize())]->hasPlayer()){
                if (observation->getCells()[c + l * std::get<0>(observation->getSize())]->getPlayer()->getRole() == SEEKER){
                    lastSeekerPos = observation->getCells()[c + l * std::get<0>(observation->getSize())]->getPlayer()->getLocation();
                    lastSeekerSeen = observation->getCells()[c + l * std::get<0>(observation->getSize())]->getPlayer();
                    return true;
                }
            }
        }
    }
    return false;
}