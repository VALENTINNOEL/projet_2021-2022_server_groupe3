#ifndef HIDE_AND_SEEK_HIDER_PLAYER_IA_V2_HPP
#define HIDE_AND_SEEK_HIDER_PLAYER_IA_V2_HPP

#include <string>
#include <memory>

#include "player/hider/hider.hpp"
#include "player/action.hpp"

/**
 * @brief  HiderV2 class
 *
 */

enum hiderState {
    HIDDEN, RUNNING
};

class IAHiderV2 : public Hider
{
private :
    Point lastSeekerPos;
    std::shared_ptr<Player> lastSeekerSeen;
    std::vector<std::shared_ptr<MazeCell>> memory;
    hiderState state;

public:
    IAHiderV2(const std::string &name);

    virtual const PlayerAction &play(const std::shared_ptr<Observation> &observation, const std::vector<PlayerAction> &actions) const;
    virtual void update(const std::shared_ptr<Observation> &prev, const std::shared_ptr<Observation> &next, const PlayerAction &action);
    bool seekerSeen(const std::shared_ptr<Observation> &observation);

    ~IAHiderV2();
};

#endif