#include "player/seeker/IAseekerV2.hpp"
#include "utils/randomGenerator.hpp"

#include <vector>

/**
 * @brief Construct a new IAV2Seeker:: IAV2Seeker object
 *
 *
 * @param name
 */
IAV2Seeker::IAV2Seeker(const std::string &name) : Seeker(name){
    lastHiderPos.x = 0;
    lastHiderPos.y = 0;
    state = DISCOVERING;
}


/**
 * @brief Destroy the Seeker:: Seeker object
 *
 */
IAV2Seeker::~IAV2Seeker() {}

/**
 * @brief Return a random action to do
 *
 * @param state
 * @return Action
 */
const PlayerAction &IAV2Seeker::play(const std::shared_ptr<Observation> &observation, const std::vector<PlayerAction> &actions) const {
    std::shared_ptr<RandGenerator> rGenerator = RandGenerator::getInstance();

    if (state == SLAYING){
        for(int l = 0; l < std::get<0>(observation->getSize()); l++){
            for(int c = 0; c < std::get<1>(observation->getSize()); c++){
                if(memory[c + l * std::get<0>(observation->getSize())]->hasPlayer()){
                    if (memory[c + l * std::get<0>(observation->getSize())]->getPlayer()->getRole() == HIDER){
                        Point hiderPos = memory[c + l * std::get<0>(observation->getSize())]->getPlayer()->getLocation();
                        for(auto &act : actions){
                            if (act.interaction == MOVE){
                                if(hiderPos.y < getLocation().y && act.orientation == TOP) return act;
                                if(hiderPos.y > getLocation().y && act.orientation == BOTTOM) return act;
                                if(hiderPos.x < getLocation().x && act.orientation == LEFT) return act;
                                if(hiderPos.x > getLocation().x && act.orientation == RIGHT) return act;
                            }
                        }
                    }
                }
            }
        }
    }

    return actions[rGenerator->getRandomInt(actions.size() - 1)];
}


void IAV2Seeker::update(const std::shared_ptr<Observation> &prev, const std::shared_ptr<Observation> &next, const PlayerAction &action) {
    // Do something...
    memory = next->getCells();

    if (hiderSeen(next)){
        state = SLAYING;
    }else if(hiderSeen(prev) && !hiderSeen(next)){
        memory[lastHiderPos.x + lastHiderPos.y * std::get<0>(next->getSize())]->setPlayer(lastHiderSeen);
        state = SLAYING;
    }else if(!hiderSeen(next) && lastHiderSeen != nullptr){
        next->getCells()[lastHiderPos.x + lastHiderPos.y * std::get<0>(next->getSize())]->setPlayer(lastHiderSeen);
        state = SLAYING;
    }else{
        state = DISCOVERING;
    }

    if(getLocation().x == lastHiderPos.x && getLocation().y == lastHiderPos.y){
        state = DISCOVERING;
        lastHiderPos.x = lastHiderPos.y = 0;
        lastHiderSeen = nullptr;
    }
};

bool IAV2Seeker::hiderSeen(const std::shared_ptr <Observation> &observation) {
    for(int l = 0; l < std::get<0>(observation->getSize()); l++) {
        for (int c = 0; c < std::get<1>(observation->getSize()); c++) {
            if(observation->getCells()[c + l * std::get<0>(observation->getSize())]->hasPlayer()){
                if (observation->getCells()[c + l * std::get<0>(observation->getSize())]->getPlayer()->getRole() == HIDER){
                    lastHiderPos = observation->getCells()[c + l * std::get<0>(observation->getSize())]->getPlayer()->getLocation();
                    lastHiderSeen = observation->getCells()[c + l * std::get<0>(observation->getSize())]->getPlayer();
                    return true;
                }
            }
        }
    }
    return false;
}