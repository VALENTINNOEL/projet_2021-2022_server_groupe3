#ifndef HIDE_AND_SEEK_SEEKER_PLAYER_IAV2_HPP
#define HIDE_AND_SEEK_SEEKER_PLAYER_IAV2_HPP

#include <string>
#include <memory>

#include "player/seeker/seeker.hpp"
#include "maze/maze.hpp"

/**
 * @brief IAV1 Seeker class
 *
 */

enum seekerState{
    DISCOVERING, SLAYING
};

class IAV2Seeker : public Seeker
{
private:
    Point lastHiderPos;
    std::shared_ptr<Player> lastHiderSeen;
    seekerState state;
    std::vector<std::shared_ptr<MazeCell>> memory;

public:
    IAV2Seeker(const std::string &name);

    virtual const PlayerAction &play(const std::shared_ptr<Observation> &observation, const std::vector<PlayerAction> &actions) const;
    virtual void update(const std::shared_ptr<Observation> &prev, const std::shared_ptr<Observation> &next, const PlayerAction &action);

    //Retourne si un hider a été vu et enregistre sa position
    bool hiderSeen(const std::shared_ptr<Observation> &obs);

    ~IAV2Seeker();
};

#endif