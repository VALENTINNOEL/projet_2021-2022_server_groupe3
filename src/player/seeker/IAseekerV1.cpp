#include "player/seeker/IAseekerV1.hpp"
#include "utils/randomGenerator.hpp"

#include <vector>

/**
 * @brief Construct a new IAV1Seeker:: IAV1Seeker object
 *
 *
 * @param name
 */
IAV1Seeker::IAV1Seeker(const std::string &name) : Seeker(name){}


/**
 * @brief Destroy the Seeker:: Seeker object
 *
 */
IAV1Seeker::~IAV1Seeker() {}

/**
 * @brief Return a random action to do
 *
 * @param state
 * @return Action
 */
const PlayerAction &IAV1Seeker::play(const std::shared_ptr<Observation> &observation, const std::vector<PlayerAction> &actions) const {

    std::shared_ptr<RandGenerator> rGenerator = RandGenerator::getInstance();

    for(int l = 0; l < std::get<0>(observation->getSize()); l++){
        for(int c = 0; c < std::get<1>(observation->getSize()); c++){
            if(observation->getCells()[c + l * std::get<0>(observation->getSize())]->hasPlayer()){
                if (observation->getCells()[c + l * std::get<0>(observation->getSize())]->getPlayer()->getRole() == HIDER){
                    for(auto &act : actions){
                        if(act.orientation == getOrientation() && act.interaction == MOVE) return act;
                    }
                }
            }
        }
    }
    return actions[rGenerator->getRandomInt(actions.size() - 1)];
}


void IAV1Seeker::update(const std::shared_ptr<Observation> &prev, const std::shared_ptr<Observation> &next, const PlayerAction &action) {
    // Do something...
};