#ifndef HIDE_AND_SEEK_SEEKER_PLAYER_IAV1_HPP
#define HIDE_AND_SEEK_SEEKER_PLAYER_IAV1_HPP

#include <string>
#include <memory>

#include "player/seeker/seeker.hpp"
#include "maze/maze.hpp"

/**
 * @brief IAV1 Seeker class
 *
 */
class IAV1Seeker : public Seeker
{
public:
    IAV1Seeker(const std::string &name);

    virtual const PlayerAction &play(const std::shared_ptr<Observation> &observation, const std::vector<PlayerAction> &actions) const;
    virtual void update(const std::shared_ptr<Observation> &prev, const std::shared_ptr<Observation> &next, const PlayerAction &action);

    ~IAV1Seeker();
};

#endif